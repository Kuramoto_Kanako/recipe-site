class CreateRecipeprocesses < ActiveRecord::Migration[5.2]
  def change
    create_table :recipeprocesses do |t|
      t.string :process_img
      t.string :process_txt
      t.integer :recipe_id

      t.timestamps
    end
  end
end
