class AddTimeToRecipe < ActiveRecord::Migration[5.2]
  def change
    add_column :recipes, :cook_min, :integer, default: 0
  end
end
