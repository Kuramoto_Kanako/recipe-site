# -*- encoding: utf-8 -*-
# stub: bootstrap-tooltip-rails 0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "bootstrap-tooltip-rails".freeze
  s.version = "0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Brandon Hilkert".freeze]
  s.date = "2012-02-01"
  s.description = "A Ruby Gem that embeds the code necessary to easily use Twitter's Bootstrap Tooltip library within your application.".freeze
  s.email = ["brandonhilkert@gmail.com".freeze]
  s.homepage = "".freeze
  s.rubyforge_project = "bootstrap-tooltip-rails".freeze
  s.rubygems_version = "2.7.6".freeze
  s.summary = "Adds Twitter's Bootstrap Tooltips to Rails".freeze

  s.installed_by_version = "2.7.6" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<thor>.freeze, [">= 0"])
      s.add_runtime_dependency(%q<rails>.freeze, [">= 3.1"])
    else
      s.add_dependency(%q<bundler>.freeze, [">= 0"])
      s.add_dependency(%q<thor>.freeze, [">= 0"])
      s.add_dependency(%q<rails>.freeze, [">= 3.1"])
    end
  else
    s.add_dependency(%q<bundler>.freeze, [">= 0"])
    s.add_dependency(%q<thor>.freeze, [">= 0"])
    s.add_dependency(%q<rails>.freeze, [">= 3.1"])
  end
end
