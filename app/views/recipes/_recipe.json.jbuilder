json.extract! recipe, :id, :user_id, :title, :created_at, :updated_at
json.url recipe_url(recipe, format: :json)
