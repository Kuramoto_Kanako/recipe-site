module FavoritesHelper

  def favorite_icon(recipe, user)
    if user
      if user.favorites.exists?(recipe: recipe)
        '<i class="fas fa-heart"></i>'
      else
        '<i class="far fa-heart"></i>'
      end
    end
  end

end
