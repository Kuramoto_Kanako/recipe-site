class FavoritesController < ApplicationController
  def toggleFav
    recipe_id = params[:recipe_id]
    user_favorites = current_user.favorites

    if user_favorites.exists?(recipe: recipe_id)
      user_favorites.where(recipe: recipe_id).destroy_all
    else
      user_favorites.create(user: current_user, recipe: Recipe.find(recipe_id))
      
    end

    render :nothing => true
  end
end
