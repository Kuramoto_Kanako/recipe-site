class Recipe < ApplicationRecord
  mount_uploader :img, ImgUploader

  belongs_to :user

  has_many :zairyos, inverse_of: :recipe, dependent: :destroy
  accepts_nested_attributes_for :zairyos, allow_destroy: true

  has_many :recipekinds, dependent: :destroy
  accepts_nested_attributes_for :recipekinds, allow_destroy: true

  has_many :recipeprocesses, inverse_of: :recipe, dependent: :destroy
  accepts_nested_attributes_for :recipeprocesses, allow_destroy: true

  has_many :favorites, dependent: :destroy
end
