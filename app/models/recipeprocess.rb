class Recipeprocess < ApplicationRecord
  mount_uploader :process_img, ImgUploader
  belongs_to :recipe
end
